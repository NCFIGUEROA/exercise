
Para el uso de este proyecto es necesario hacer la configuración correspondiste para el uso del framework 
Laravel 6.2, tal como tener instalado composer, un servidor web Apache mediante XAMPP y crear el proyecto de laravel.

1.- El archivo '.env.example' se debe renombrar a '.env', aqui se configura el environment de la aplicación, 
    como las credenciales para la conección a la base de datos.
2.- Correr en consola los comandos, 'composer install' y 'php artisan key:generate'.
3.- Crear base de datos en formato utf8mb4_general_ci con el nombre 'exercise'.
4.- Correr migraciones en la base de datos con el comando 'php artisan migrate'.
5.- Correr en consola el comando: 'php artisan db:seed --class=DatabaseSeeder' , para llenar las tablas 'role' y 'permission' respectivamente.
6.- Leer la documentación para el uso de la API REST con el siguiente link: https://documenter.getpostman.com/view/6847286/SW15yGdr
    en donde se muestra como implementar cada endpoint de la API. 