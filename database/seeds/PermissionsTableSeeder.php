<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'permission' => 'Crear',
        ]);
        DB::table('permissions')->insert([
            'permission' => 'Leer',
        ]);
        DB::table('permissions')->insert([
            'permission' => 'Actualizar',
        ]);
        DB::table('permissions')->insert([
            'permission' => 'Eliminar',
        ]);
        DB::table('permissions')->insert([
            'permission' => 'Copiar ',
        ]);
        DB::table('permissions')->insert([
            'permission' => 'Activar',
        ]);
    }
}
