<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/users', 'UserController@store');
Route::get('/users', 'UserController@index');
Route::get('/users/status/{status}', 'UserController@getUsersByStatus');
Route::get('/users/role/{roleId}', 'UserController@getUserByRole');
Route::get('/users/permission/{permissionId}', 'UserController@getUserByPermission');
Route::get('/users/{id}', 'UserController@getUserById');
Route::put('/users/update/{id}', 'UserController@update');
Route::delete('/users/delete/{id}', 'UserController@destroy');
