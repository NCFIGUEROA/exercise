<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\RolePermission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
//        dd($user);
        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->validatorUserStore($request->all())->fails()) {
            return response()->json(["message" => $this->validatorUserStore($request->all())->getMessageBag()], 400);
        }

        $itemsUser = [
            'username' => $request->username,
            'email' => $request->email,
            'names' => $request->names,
            'paternal_surname' => $request->paternal_surname,
            'maternal_surname' => $request->maternal_surname,
            'age' => $request->age,
            'status' => true,
        ];

        $user = User::create($itemsUser);

       foreach ($request->permissions as $permission){
           RolePermission::create([
               'role_id' => $request->role,
               'permission_id' => $permission,
               'user_id' => $user->id
           ]);
       }

       return response()->json([
           "message" => "The user had been registered",
           "user" => $itemsUser
       ], 200);
    }

    protected function validatorUserStore(array $data)
    {
        $messages = [
            'username.required' => 'Username field is required',
            'username.unique' => 'Username field already exists',
            'email.required' => 'Email field is required',
            'email.unique' => 'Email field already exists',
            'names.required' => 'Names field is required',
            'paternal_surname.required' => 'Paternal surname field is required',
            'role.required' => 'Role field is required',
            'permissions.required' => 'Permissions field is required',
            'permissions.array' => 'Permission should be an array'
        ];

        return Validator::make($data, [
            'username' => 'required|max:150|unique:users',
            'email' =>'required|max:120|unique:users',
            'names' =>'required|max:100',
            'paternal_surname' =>'required|max:50',
            'role' => 'required',
            'permissions' => 'required|array'
        ], $messages);
    }


    public function getUserByPermission ($permissionId) {
        $rolePermission = RolePermission::where('permission_id', '=', $permissionId)->get();
        $rolePermission->load('user');

        $users = [];

        $permissionName = Permission::find($permissionId);
        foreach ($rolePermission as $permission) {
            $permission->user->permission = $permissionName->permission;
            array_push($users, $permission->user);
        }

        $usersCollection = collect($users);
        $usersFilter = $usersCollection->unique('id');

        return response()->json([
            "users" => $usersFilter->values()->all()
        ], 200);
    }

    public function getUsersByStatus ($status) {
        $users = User::where('status', '=', $status === 'active')->get();

        return response()->json([
            "users" => $users
        ], 200);
    }

    public function getUserByRole ($roleId) {
        $rolePermission = RolePermission::where('role_id', '=', $roleId)->get();
        $rolePermission->load('user');

        $users = [];
        $roleName = Role::find($roleId);
        foreach ($rolePermission as $role) {
            $role->user->role = $roleName->role;
            array_push($users, $role->user);
        }

        $usersCollection = collect($users);
        $usersFilter = $usersCollection->unique('id');

        return response()->json([
            "users" => $usersFilter->values()->all()
        ], 200);
    }

    public function getUserById ($id) {
        $user = User::find($id);
//        dd($user);
        return response()->json([
            "user" => $user
        ], 200);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validatorUserUpdate($request->all())->fails()) {
            return response()->json(["message" => $this->validatorUserUpdate($request->all())->getMessageBag()], 400);
        }
//        dd($request->all());
        User::find($id)->update($request->all());
        return response()->json([
            "message" => "The user had been updated"
        ], 200);
    }
    protected function validatorUserUpdate(array $data)
    {
        $messages = [
            'username.unique' => 'Username field already exists',
            'email.unique' => 'Email field already exists',
            'permissions.array' => 'Permission should be an array'
        ];

        return Validator::make($data, [
            'username' => 'max:150|unique:users',
            'email' =>'max:120|unique:users',
            'names' =>'max:100',
            'paternal_surname' =>'max:50',
            'role' => '|max:3',
            'permissions' => 'array'
        ], $messages);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json([
            "message" => "The user had been deleted",
        ], 200);
    }
}
